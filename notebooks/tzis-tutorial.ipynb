{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "d8957c6c-7375-4bff-8985-475be95a245f",
   "metadata": {},
   "source": [
    "# Tzis -  *T*o *Z*arr *i*n *S*torages\n",
    "\n",
    "`tzis` is a small python package originally designed for\n",
    "\n",
    "1. converting data into the [zarr](https://zarr.readthedocs.io/en/stable/) format and\n",
    "1. writing it to the DKRZ's cloud storage space [swift](https://swiftbrowser.dkrz.de/)\n",
    "\n",
    "in one step. It is based on a script which uses [xarray](http://xarray.pydata.org/en/stable/index.html), the `fsspec` [implementation for swift](https://github.com/d70-t/swiftspec) from Tobias Kölling and the [dask](https://docs.dask.org/en/stable/) distributed scheduler. `tzis` is optimized for DKRZ's High Performance Computer but can also be used from local computers."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5ae0977b-54a0-48d0-bb20-f8f370aa2f4a",
   "metadata": {},
   "source": [
    "`tzis` features\n",
    "\n",
    "- **Parallel writing** of a zarr dataset which contains many files per `write_zarr` call with `dask` .\n",
    "- **Avoid intermediate writing** to temporary storage for e.g. reformatting purposes because tzis directly writes the dataset into the target storage.\n",
    "- **Appending** variables to existing zarr datasets which allows to add grids afterwards\n",
    "- Optimized **Rechunking** along one dimension such that chunk sizes match quota of the target storage and dask graphs are kept small\n",
    "- **Consolidated metadata** by saving the intersection of many files' metadata into one .zmetadata json file.\n",
    "- **All kind of other target storage**s (filesystem, s3, gcs, zip,...) by setting the `.fsspec_map` attribute manually.\n",
    "- Writing of **different input file formats**. \\*\n",
    "- **Modular design**: Common feel from fsspec and xarray. Import only what you need:\n",
    "    - **Swiftspec** implementation for `fsspec` enabling linux-like file system operations on the object store (like `listdir`)\n",
    "    - **Provenance**: Provencance files based on the `prov` lib are saved inside the container in the virtual directory `provenance` for all `write_zarr` calls.\n",
    "    - **Catalog creation**: Make your zarr datasets easy browsable and accessible via `intake` catalogs\n",
    "\n",
    "\\* All files that can be passed to \n",
    "```python\n",
    "xarray.open_mfdataset()\n",
    "```\n",
    "can be used.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6f216a5e-dd27-42ad-ad9b-baa4b0434f86",
   "metadata": {},
   "source": [
    "In this tutorial, you will learn\n",
    "\n",
    "- the [meaning](#define) of `zarr` and the `swift object storage`\n",
    "- why you can [benefit](#moti) from `zarr` in cloud storage\n",
    "- [when](#when) it is a good idea to write into cloud\n",
    "- how to [initializie the swift store](#token) for `tzis` including creating a token\n",
    "- how to [open and configure](#source) the source dataset\n",
    "- how to [write](#write) data to swift\n",
    "- how to [set options](#output) for the zarr output\n",
    "- how to [access](#access) and use data from swift\n",
    "- how to work with the [SwiftStore](#swiftstore) similar to file systems"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8d6a1c4f-66b5-466a-84b6-8a011ac5bd82",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"define\"></a>\n",
    "\n",
    "# Definition\n",
    "\n",
    "**Zarr** is a *cloud-optimised* format for climate data. By using *chunk*-based data access, `zarr` enables arrays the can be larger than memory. Both input and output operations can be parallelised. It features *customization* of compression methods and stores. \n",
    "\n",
    "The **Swift** cloud object storage is a 🔑 *Keyvalue* store where the key is a global unique identifier and the value a representation of binary data. In contrast to a file system 📁 , there are no files or directories but *objects and containers/buckets*. Data access is possible via internet i.e. `http`."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "82d19de7-889b-423a-beb2-0ed10972c90e",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"moti\"></a>\n",
    "\n",
    "# Motivation\n",
    "\n",
    "In recent years, cloud object storage systems became an alternative to traditional file systems because of\n",
    "\n",
    "- **Independency** from computational ressources. Users can access and download data from anywhere without the need of HPC access or resources\n",
    "- **Scalability** because no filesystem has to care about the connected disks.\n",
    "- **A lack of storage** space in general because of increasing model output volume.\n",
    "- **No namespace conflicts** because data is accessed via global unique identifier\n",
    "\n",
    "Large Earth System Science data bases like the CMIP Data Pool at DKRZ contain [netCDF](https://github.com/Unidata/netcdf-c) formatted data. Access and transfers of such data from an object storage can only be conducted on file level which results in heavy download volumes and less reproducible workflows. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "58a88ea9-8d3c-4e6c-8369-ce890a66c495",
   "metadata": {},
   "source": [
    "The cloud-optimised climate data format [Zarr](https://zarr.readthedocs.io/en/stable/) solves these problems by\n",
    "\n",
    "- enabling **flexible and modular** compression, storage and storage protocol combinations\n",
    "- allowing **asyncronous work** so that users can work on not-finished output\n",
    "- allowing programs to identify _chunks_ corresponding to the desired subset of the data before the download so that the **volume of data transfer is reduced**.\n",
    "- allowing users to access the data via `http` so that both **no authentication** or software on the cloud repository site is required \n",
    "- saving **meta data** next to the binary data. That allows programs to quickly create a virtual representation of large and complex datasets.\n",
    "\n",
    "Zarr formatted data in the cloud makes the data as *analysis ready* as possible.\n",
    "\n",
    "With `tzis`, we developed a package that enables to use DKRZ's insitutional cloud storage as a back end storage for Earth System Science data. It combines `swiftclient` based scripts, a *Zarr storage* implementation and a high-level `xarray` application including `rechunking`. Download velocity can reach the order of **1000 MB/s** if the network allows it. Additional validation of the data transfer ensures its completeness."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "87d9ffaf-55c4-4013-b961-3bfb94d9c99a",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"when\"></a>\n",
    "\n",
    "# Which type of data is suitable?\n",
    "\n",
    "Datasets in the cloud are useful if\n",
    "\n",
    "- the cloud place can be their final location. Moving data in the cloud is very inefficient.\n",
    "- they will not be *prepended*. Data in the cloud can be easily *appended* but *prepending* most likely requires moving which is not efficient.\n",
    "- they are *open*. One advantage comes from the easy access via http. This is even easier when useres do not have to log in."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "77feab5f-a512-4449-95c7-4daa0762f25f",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"token\"></a>\n",
    "\n",
    "# Swift authentication and initialization\n",
    "\n",
    "Central `tzis` functions require that you specify an `OS_AUTH_TOKEN` which allows the program to connect to the swift storage with your credentials. This token is valid for a month per default. Otherwise, you would have to login for each new session. When you work with `swift`, this token is saved in the hidden file `~/.swiftenv` which contains the following paramter\n",
    "\n",
    "- `OS_STORAGE_URL` which is the URL associated with the storage space of the project-user-account. Note that this URL cannot be opened like a *swiftbrowser* link but instead it can be used within programs like `tzis`. Only one URL is saved so that you have to reconfigure if you would like to use another account.\n",
    "- `OS_AUTH_TOKEN`. \n",
    "\n",
    "**Be careful** with the token. It should stay only readable for you. Especially, do not push it into git repos."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aa1c7b7f-6c8e-44db-9c29-7bb9eee88a84",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"token\"></a>\n",
    "\n",
    "## `swifthandling`: Get token and url\n",
    "\n",
    "`Tzis` includes a function to get the token or, if not available, create the token:\n",
    "\n",
    "```python\n",
    "from tzis import swifthandling\n",
    "token=swifthandling.get_token(host, account, username=USERNAME)\n",
    "```\n",
    "\n",
    "where `host` is either \"dkrz\" or \"jsc\".\n",
    "When calling `get_token`,\n",
    "\n",
    "1. it tries to read in the configuration file `~/.swiftenv`\n",
    "1. if there is a file, it checks, if the found configuration matches the specified *account*\n",
    "1. if no file was found or the configuration is invalid, it will create a token\n",
    "    1. it asks you for a password\n",
    "    1. it writes two files: the `~/.swiftenv` with the configuration and `~/.swiftenv_useracc` which contains the account and user specification for that token.\n",
    "1. it returns a dictionary with all configuration variables"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "13396f94-6da3-4eb9-9602-1045fc9540c5",
   "metadata": {},
   "source": [
    "# Initialize swift mapper \n",
    "\n",
    "After successfully creating the authentication for swift, we *initialize* a swift container in which we will save the data. We do that with\n",
    "\n",
    "```python\n",
    "fsmap=swifthandling.get_swift_mapper(\n",
    "    os_url,\n",
    "    os_token,\n",
    "    os_container,\n",
    "    os_name=os_name\n",
    ")\n",
    "```\n",
    "\n",
    "The mandatory arguments are:\n",
    "\n",
    "- `os_url` is the `OS_STORAGE_URL`\n",
    "- `os_token` is the `OS_AUTH_TOKEN`\n",
    "- `os_container` is the *container name* / the *bucket*. A container is the highest of two store levels in the swift object store.\n",
    "\n",
    "these will connect you to the swift store and initialize/create a container.\n",
    "\n",
    "You can\n",
    "\n",
    "- already specify a `os_name` which is the *zarr dataset name* or the *object* name where the data will be in the end. \n",
    "- decide whether you want to run the write process in *Verbose* mode by specifying `verbose=True`\n",
    "\n",
    "E.g.:\n",
    "\n",
    "```python\n",
    "fsmap=swifthandling.get_swift_mapper(\n",
    "    token[\"OS_STORAGE_URL\"],\n",
    "    token[\"OS_AUTH_TOKEN\"],\n",
    "    \"tzistest\",\n",
    "    os_name=\"zarrfile\"\n",
    ")\n",
    "```\n",
    "\n",
    "You can check if your mapper works by applying\n",
    "\n",
    "```python\n",
    "list(fsmap.keys())\n",
    "```\n",
    "\n",
    "which will print *all* existing elements in the container."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c2380bfc-22ab-41c0-96a6-aa9ce2203b13",
   "metadata": {},
   "source": [
    "# Setting a zarr dataset name (an object prefix)\n",
    "\n",
    "You can switch to different zarr dataset output names within one container by using fsspec:\n",
    "\n",
    "```python\n",
    "zarr_dset_name=\"test\"\n",
    "fsmap=fsspec.get_mapper(\n",
    "    '/'.join(\n",
    "        [fsmap.root, #Or use `os.path.dirname(fsmap.root)`  if you set another object prefix already\n",
    "         zarr_dset_name\n",
    "        ]\n",
    "    )\n",
    "```\n",
    "\n",
    "Use `os.path.dirname(fsmap.root)`  instead of `fsmap.root` if you set another object prefix already."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9ec4e347-4d16-4916-8cd0-355ddd512fe2",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"source\"></a>\n",
    "\n",
    "# `openmf`: Open and configure the source dataset\n",
    "\n",
    "We can use `open_mfdataset_optimized` from module `tizs.openmf` for opening the dataset. `tzis` uses `xarray`'s `open_mfdataset` for reading the source file(s). The corresponding call is:\n",
    "\n",
    "```python\n",
    "def open_mfdataset_optimize(\n",
    "    mf,\n",
    "    varname,\n",
    "    target_fsmap,\n",
    "    chunkdim=None,\n",
    "    target_mb=None,\n",
    "    xarray_kwargs=None,\n",
    "    verbose=False\n",
    ")\n",
    "```\n",
    "\n",
    "E.g.:\n",
    "\n",
    "```python\n",
    "from tzis import openmf\n",
    "varname=\"tas\"\n",
    "glob_path_to_dataset = f\"/work/ik1017/CMIP6/data/CMIP6/ScenarioMIP/DKRZ/MPI-ESM1-2-HR/ssp370/r1i1p1f1/Amon/{varname}/gn/v20190710/*\"\n",
    "omo=openmf.open_mfdataset_optimize(\n",
    "    glob_path_to_dataset,\n",
    "    varname,\n",
    "    fsmap,\n",
    "    chunkdim=\"time\",\n",
    "    target_mb=100,\n",
    "    verbose=True\n",
    ")\n",
    "omo\n",
    "```\n",
    "\n",
    "Note that the actual dataset can be retrieved via `omo.mf_dset`."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "41f810c1-17ec-4d94-97ae-2cbf90a90013",
   "metadata": {},
   "source": [
    "It uses a configuration which tries to **merge** many source files into one dataset **without producing errors**. Therefore, *conflicts* are ignored and only structures and attributes are combined which are overcutting in all source files. The internal function looks like: \n",
    "\n",
    "```python\n",
    "OPEN_MFDATASET_KWARGS = dict(\n",
    "    decode_cf=True,\n",
    "    use_cftime=True,\n",
    "    data_vars=\"minimal\",\n",
    "    coords=\"minimal\",\n",
    "    compat=\"override\",\n",
    "    combine_attrs=\"drop_conflicts\",\n",
    ")\n",
    "\n",
    "mf_dset = xarray.open_mfdataset(mf,\n",
    "                               **OPEN_MFDATASET_KWARGS)\n",
    "```\n",
    "\n",
    "- Additionally, tzis will try to grep the **tracking_id** attributes of all input files and save them in the **provenance**. If none are available, the **name** of the files given in `mf` are used for provenance instead.\n",
    "- The **history** attribute of the target zarr data set will be set to `f'Converted and written to swift cloud with tzis version {tzis.__version__}'`\n",
    "- An **optimized** chunk size is used already for opening the dataset in order to match the specified target chunk size"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "565f0ac4-eed3-4212-b761-09c36b9703e3",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Attributes\n",
    "\n",
    "*Attributes* of the dataset are handled in a `dict`ionary in the `omo.mf_dset` variable via `xarray`. You can **add** or **delete** attributes just like items from a dictionary:\n",
    "\n",
    "```python\n",
    "#add an attribute\n",
    "omo.mf_dset.attrs[\"new_attribute\"]=\"New value of attribute\"\n",
    "print(omo.mf_dset.attrs[\"new_attribute\"])\n",
    "\n",
    "#delete the attribute\n",
    "del omo.mf_dsetf.attrs[\"new_attribute\"]\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6736e17c-c4e5-4393-8ef9-13590c2397fe",
   "metadata": {},
   "source": [
    "## Grib input\n",
    "\n",
    "If you want to use `grb` input files, you can specify `cfgrib` as an **engine** for `xarray`.\n",
    "```python\n",
    "openmf.open_mfdataset_optimize(list_of_grib_files, \"pr\", xarray_kwargs=dict(engine=\"cfgrib\"))\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e638da79-4cae-4db3-bfaf-398174dd13dd",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Chunking and rechunking - best practices\n",
    "\n",
    "A correct chunk configuration is **essential**  to enable users to access, analysis and download zarr data with the **highest performance** possible. With `tzis`, you can set\n",
    "\n",
    "- `chunkdim`: the dimension, which is used for rechunking\n",
    "- `target_mb`: the target size of one chunk\n",
    "\n",
    "To match the `target_mb`, tzis estimates the resulting chunk sizes by using the size of the array of the variable. <mark> However, the compression ratio can only rarely be taken into account. </mark> If the size of the source dataset can be retrieved via `fsspec`, an estimated compression ratio from the source is applied on the target chunk size."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2db64ae8-6e2d-4b07-a171-4ca36cfd3c65",
   "metadata": {
    "tags": []
   },
   "source": [
    "### Chunk sizes\n",
    "\n",
    "<div class=\"alert alert-block alert-info\">\n",
    "    <b> Note: </b> If you set target_mb=0, rechunking is set off. That also allows to use multiple chunk dimensions with a corresponding dask setting (see arguments for open_mfdataset) and tzis is able to write them to the final zarr dataset.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e9c5d5e5-564a-4de1-97b1-e2a09801e035",
   "metadata": {
    "tags": []
   },
   "source": [
    "The size of a chunk has technical limitations on both interval limits:\n",
    "\n",
    "- Sizes **larger than 2GB** are not supported by swift\n",
    "- Chunks **smaller than 10 MB** may lead to problems when writing the data due to many small `PUT` requests.\n",
    "\n",
    "In between this interval, the size optimization corresponds to network fine adjustment between **latency** and traffic **volume**.\n",
    "\n",
    "- If users are interested in accessing and downloading the **entire** dataset, you should set the `target_mb` to the maximum in order to **reduce latency**. E.g., writing zarr data for DKRZ's HSM archive should be done with maximal `target_mb`.\n",
    "- If users are interested in **small subsets** of the data, **lower the `target_mb`** so that **volume** is reduced.\n",
    "\n",
    "If both use cases are possible, choose a chunk size in between. The highest **throughput** rates ( **O(1GB/s)** ) can be acchieved when the chunk size is over **100 MB**, however, this requires high network volume.\n",
    "\n",
    "<div class=\"alert alert-block alert-info\">\n",
    "    <b> Note: </b> If the rechunk-dimension is not `time` and multiple files were opened for the input dataset,\n",
    "    the zarr output will have the addition chunking-dimensions `time`.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f55d0fea-e94a-48c3-8499-3e56f71d83ea",
   "metadata": {
    "tags": []
   },
   "source": [
    "### Chunk dimension\n",
    "\n",
    "Chunk dimension configuration highly depend on the use cases of the final zarr data:\n",
    "\n",
    "- if users mainly subselect on spatial dimensions rather than temporal, chunk over a spatial dimension (**lat** or **lon**). A use case can be **calculating climate indices** for a special location.\n",
    "- if users mainly subselect on temporal dimension rather than spatial, chunk over time. This is recommended if you have long simulations covering many years on high frequencies.\n",
    "\n",
    "It also depends on the shape of the input arrays:\n",
    "\n",
    "- if the data is available on an **high resolution unstructured grid** i.e. on a **long and single spatial dimension**, it might be worth to consider rechunking over this axis. This can be tested e.g. for high resoltion ICON simulations.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "49c056f9-f038-496f-b3cc-3bb16ba41ae4",
   "metadata": {
    "tags": []
   },
   "source": [
    "# Rechunking\n",
    "\n",
    "For *rechunking* an xarray dataset along chunk dimension `chunkdim` by using variable `varname` and target chunk size `target_mb`, you can use `rechunk` from tzis.rechunker:\n",
    "\n",
    "```python\n",
    "from tzis import rechunker\n",
    "ds_rechunked=rechunker.rechunk(\n",
    "    ds,\n",
    "    varname,\n",
    "    chunkdim,\n",
    "    target_mb,\n",
    "    verbose\n",
    ")\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "76171636-1f6c-453b-942e-c62d2b49467d",
   "metadata": {
    "tags": []
   },
   "source": [
    "<a class=\"anchor\" id=\"write\"></a>\n",
    "\n",
    "# Writing to swift\n",
    "\n",
    "After we have\n",
    "\n",
    "1. initialized the container \n",
    "1. opened or rechunked the dataset\n",
    "\n",
    "we can **write** it into cloud. The conversion to `zarr` is made on the way. We can specify all necessary configuration options within the `write` function:\n",
    "\n",
    "```python\n",
    "def write_zarr(\n",
    "    self,\n",
    "    fsmap,\n",
    "    mf_dset,\n",
    "    varname,\n",
    "    verbose=False,\n",
    "    chunkdim=\"time\",\n",
    "    target_mb=0,\n",
    "    startchunk=0,\n",
    "    validity_check=False,\n",
    "    maxretries=3,\n",
    "    trusted=True\n",
    ")\n",
    "```\n",
    "\n",
    "The function allows you\n",
    "\n",
    "- to set `chunkdim` which is the *dimension* used for chunking.\n",
    "- to set the target size `target_mb` of a data chunk. A *chunk* corresponds to an object in the swift object storage.\n",
    "- to set the `startchunk`. If the write process was interrupted - e.g. because your dataset is very large,  you can specify at which chunk the write process should restart.\n",
    "- to set the number of *retries* if the transfer is interrupted.\n",
    "- to set `validity_check=True` which will validate the transfer **instead of writing** the data. This checks if the data in the chunks are equal to the input data.\n",
    "- to set `trusted=False` which validates the transfer **after writing** the data into cloud. The validation is equal to `validity_check=True`\n",
    "\n",
    "E.g.\n",
    "\n",
    "```python\n",
    "from tzis import tzis\n",
    "outstore=tzis.write_zarr(\n",
    "    omo.target_fsmap,\n",
    "    omo.mf_dset,\n",
    "    omo.varname,\n",
    "    verbose=True,\n",
    "    target_mb=0\n",
    ")\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9f045270-f61d-450d-8bc5-dd9a725c7dfb",
   "metadata": {
    "jp-MarkdownHeadingCollapsed": true,
    "tags": []
   },
   "source": [
    "The output `outstore` of `write_zarr` is a new variable for the output **zarr storage**. Packages like `xarray` which are using `zarr` can identify and open the *consolidated* dataset from the cloud with that store. The `os_name` of `container` can now be changed while the `outstore` still points to the written `os_name`."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5230a651-4f6d-4c12-a0d1-bb9bb790877d",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Parallelization\n",
    "\n",
    "Dask is used to parallelize the `write_zarr` function. Tzis tries to get a running client with dask's `get_client` function. If that fails, it opens a local client with half of the maximum available CPU and memory resources. \n",
    "\n",
    "Dask will process a default batch of 70 chunks at once. Most of the time, tzis resizes the batch so that it is most likely that the memory is not overloaded."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e494d109-82fa-448b-ac0d-ce4f77565949",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Compression\n",
    "\n",
    "Compression configuration can be set from outside of `tzis` via the `zarr` and `numcodecs` libs.\n",
    "\n",
    "[From Zarr docs:](https://zarr.readthedocs.io/en/v2.10.2/tutorial.html#compressors)\n",
    "\n",
    "> If you don’t specify a compressor, by default Zarr uses the [Blosc](https://github.com/Blosc) compressor. Blosc is generally very fast and can be configured in a variety of ways to improve the compression ratio for different types of data. Blosc is in fact a *“meta-compressor”*, which means that it can use a number of different compression algorithms internally to compress the data. A list of the internal compression libraries available within Blosc can be obtained via:\n",
    "\n",
    "```python\n",
    "from numcodecs import blosc\n",
    "blosc.list_compressors()\n",
    "['blosclz', 'lz4', 'lz4hc', 'snappy', 'zlib', 'zstd']\n",
    "```\n",
    "\n",
    "> The default compressor can be changed by setting the value of the zarr.storage.default_compressor variable, e.g.:\n",
    "\n",
    "```python\n",
    "import zarr.storage\n",
    "from numcodecs import Zstd, Blosc\n",
    "# switch to using Zstandard\n",
    "zarr.storage.default_compressor = Zstd(level=1)\n",
    "```\n",
    "\n",
    "> A number of different compressors can be used with Zarr. A separate package called [NumCodecs](http://numcodecs.readthedocs.io/) is available which provides a common interface to various compressor libraries including Blosc, Zstandard, LZ4, Zlib, BZ2 and LZMA. Different compressors can be provided via the compressor keyword argument accepted by all array creation functions. \n",
    "\n",
    "Alistair Miles has made a [benchmark test](http://alimanfoo.github.io/2016/09/21/genotype-compression-benchmark.html) for compression.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c020d5f4-5c91-477f-bac9-22a8debb6854",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Overwriting or appending?\n",
    "\n",
    "`write_zarr()` per default **appends** data if possible. It calls `xarray`'s `to_zarr()` function *for each chunk* by setting the `region` argument. Before a chunk is written, it is checked if there is already a chunk for exactly the **slice** of the dataset that should be written. If so, the chunk is skipped. Therefore, recalling `write_zarr` only overwrites chunks if they cover a different slice of the source dataset.\n",
    "\n",
    "<div class=\"alert alert-block alert-info\">\n",
    "    <b>Note: </b> Checking for the <b>startchunk</b> automatically ca take very long.\n",
    "    We recommend to manually set `startchunk` whenever possible in order to skip the comparison of a range of chunks.\n",
    "    Then, the function will jump to `startchunk` and start appending there.\n",
    "</div>\n",
    "\n",
    "When data is appended, the zarr dataset is opened with\n",
    "\n",
    "```python\n",
    "OPEN_ZARR_KWARGS = dict(\n",
    "    consolidated=True,\n",
    "    decode_cf=True,\n",
    "    use_cftime=True\n",
    ")\n",
    "xarray.open_zarr(omo.mf_dset,\n",
    "                 **OPEN_ZARR_KWARGS)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9a3955e2-4748-46d1-9b8d-1a755ea45024",
   "metadata": {
    "tags": []
   },
   "source": [
    "1. **Overwriting conditions**\n",
    "    - if the chunk setting for the variable which is to be written has changed\n",
    "2. **Error raising conditions**\n",
    "    - if the coordinates of the source dataset differ from the target dataset\n",
    "3. Still **Appending cases**:\n",
    "    - if the global attributes of the datasets differ. Such attributes are printed."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fb5c5296-12a4-4096-8c64-5ed02e40b4cc",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Writing or appending a second variable\n",
    "\n",
    "1. If the second variable has either coordinates with the same name but different values or different meta data, open a new prefix `new_os_name` first:\n",
    "```python\n",
    "fsmap= fsspec.get_mapper(\n",
    "    '/'.join(\n",
    "        [\n",
    "            fsmap.root, #Or use `os.path.dirname(fsmap.root)`  if you set another object prefix already\n",
    "            new_zarr_dset_name\n",
    "        ]\n",
    "    )\n",
    ")\n",
    "```\n",
    "    \n",
    "2. If necessary, rechunk the source dataset for the other variable:\n",
    "\n",
    "```python\n",
    "omo.mf_dset=rechunker.rechunk(\n",
    "    omo.mf_dset,\n",
    "    new_varname,\n",
    "    new_chunkdim,\n",
    "    new_target_mb,\n",
    "    verbose\n",
    ")\n",
    "```\n",
    "\n",
    "3. Write to the target storage:\n",
    "```python\n",
    "outstore=tzis.write_zarr()\n",
    "```\n",
    "\n",
    "The variable will be appended if possible."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "83f05439-12fe-43ac-8efa-5da0755eee6f",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Using another source dataset\n",
    "\n",
    "After initializing your container, you can overwrite its variables. E.g. by\n",
    "\n",
    "```python\n",
    "omo=openmf.open_mfdataset_optimized(\n",
    "    other_mfs_towrite,\n",
    "    varname,\n",
    "    fsmap,\n",
    "    chunkdim=\"time\",\n",
    "    target_mb=100,\n",
    "    verbose=True\n",
    ")\n",
    "```\n",
    "\n",
    "Afterwards, you can write into the same target storage by repeating your `tzis.write_zarr()` command."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f06222ba-fb91-40b8-8063-5dd88d18bc85",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Provenance\n",
    "\n",
    "The provenance created by tzis describes what the operation `write_zarr` has done:\n",
    "\n",
    "- it saves the version of the `tzis` and the application time\n",
    "- it gathers the input files and uses either their names or their `tracking_id` as identifier\n",
    "- it connects the input files to the new zarr dataset which is identified by a newly created `uuid`\n",
    "- it saves the values of the keyword arguments of the operation (\"chunkdim\", \"startchunk\", \"target_mb\"), and, if used, the keyword arguments of `tzis`'s `open_mfdataset` function.\n",
    "\n",
    "Two files are created in the virtual directory \"provenance\" in the container:\n",
    "\n",
    "- `f\"provenance_{dset}_{self.uuid}.json\"`\n",
    "- `f\"provenance_{dset}_{self.uuid}.png\"`\n",
    "\n",
    "Where `dset` is the output prefix and `self.uuid` the newly generated uuid for the zarr dataset."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b4e19440-3fd9-406f-80e2-752070e2e060",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"access\"></a>\n",
    "\n",
    "# Access and use your Zarr dataset\n",
    "\n",
    "1. You can open the *consolidated zarr datasets* with `xarray` using an URL-prefix-like string constructed as \n",
    "\n",
    "```python\n",
    "zarrinput='/'.join([OS_STORAGE_URL,os_container,os_name])\n",
    "xarry.open_zarr(zarrinput, consolidated=True, decode_times=True)\n",
    "```\n",
    "\n",
    "This is possible if the container is *public* (see *how to make a container public*).\n",
    "\n",
    "Tzis automatically generate an index file which contains URLs for all prefixes of the container. The location is \n",
    "\n",
    "```python\n",
    "'/'.join([OS_STORAGE_URL,os_container,\"INDEX.csv\"])\n",
    "```\n",
    "\n",
    "2. If your container is *private*, you have to use tokens and fsspec. If you create environment variables for OS_AUTH_TOKEN and OS_STORAGE_URL, you can get a fsspec mapper which than can be used for xarray:\n",
    "\n",
    "```python\n",
    "#note: this is working in notebooks:\n",
    "%env OS_AUTH_TOKEN={token[\"OS_AUTH_TOKEN\"]}\n",
    "%env OS_STORAGE_URL={token[\"OS_STORAGE_URL\"]}\n",
    "\n",
    "import fsspec\n",
    "targetstore = fsspec.get_mapper(zarrinput)\n",
    "zarr_dset = xarray.open_zarr(targetstore, consolidated=True, decode_times=True)\n",
    "zarr_dset\n",
    "```\n",
    "\n",
    "1. You can download data from the [swiftbrowser](https://swiftbrowser.dkrz.de) manually"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "594b8d3e-8d95-4bcd-ad0c-55baef5b7915",
   "metadata": {},
   "source": [
    "## Catalogs\n",
    "\n",
    "`tzis` features\n",
    "\n",
    "```python\n",
    "    catalog.write_catalog(\n",
    "        self.fsspec_map.fs,\n",
    "        self.fsspec_map,\n",
    "        os.basename(self.fsspec_map.root),\n",
    "        catalogname=\"catalog.json\",\n",
    "        pattern=None,\n",
    "        delim=\".\",\n",
    "        columns=[],\n",
    "        mode=\"a\",\n",
    "        verbose=self.verbose,\n",
    "    )\n",
    "```\n",
    "\n",
    "which creates an `intake` catalog named `catalogname`. This will try to collect all virtual directories on the container level and interprete them as zarr datasets. I.e., the written zarr datasets should not have a virtual drs.\n",
    "\n",
    "- `pattern` allows to subset the directories of the container by a pattern. E.g., if `pattern=\"CMIP6\"`, only those directories are selected which have `CMIP6` in their names.\n",
    "- `delim` will be used to split the directory names and put each value into a single catalog column.\n",
    "- `columns` are the headers of the columns.\n",
    "- `mode` is either `a` for append or `w` for overwrite the catalog.\n",
    "\n",
    "<div class=\"alert alert-block alert-info\">\n",
    "    <b>Note: </b> `delim` will only work when all directories have the same amounts of `delim` in their names\n",
    "    and the length of the `columns` list is equal to the amount of `delim` in the directory names.\n",
    "</div>\n",
    "\n",
    "E.g.\n",
    "\n",
    "```python\n",
    "container.write_catalog()\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c976d55c-502d-47ab-b3ef-67842f6aea11",
   "metadata": {},
   "source": [
    "## Coordinates\n",
    "\n",
    "Sometimes, you have to *reset* the coordinates because it gets lost on the transfer to zarr:\n",
    "```python\n",
    "precoords = set(\n",
    "    [\"lat_bnds\", \"lev_bnds\", \"ap\", \"b\", \"ap_bnds\", \"b_bnds\", \"lon_bnds\"]\n",
    ")\n",
    "coords = [x for x in zarr_dset.data_vars.variables if x in precoords]\n",
    "zarr_dset = zarr_dset.set_coords(coords)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "91a744a5-eb11-4d21-a2be-9f7ac7284c21",
   "metadata": {},
   "source": [
    "# Reconvert to NetCDF\n",
    "\n",
    "The basic reconversion to netCDF can be done with `xarray`:\n",
    "```python\n",
    "written.to_netcdf(outputfilename)\n",
    "```\n",
    "\n",
    "## Compression and encoding:\n",
    "\n",
    "Often, the original netCDF was compressed. You can set different compressions in an **encoding** dictionary. For using `zlib` and its compression level 1, you can set:\n",
    "\n",
    "```python\n",
    "encoding = {var: written[var].encoding for var in written.data_vars}\n",
    "var_dict = dict(zlib=True, complevel=1)\n",
    "for var in written.data_vars:\n",
    "    encoding[var].update(var_dict)\n",
    "```\n",
    "\n",
    "## FillValue\n",
    "\n",
    "`to_netcdf` might write out *FillValue*s for coordinates which is not compliant to CF. In order to prevent that, set an encoding as follows:\n",
    "\n",
    "```python\n",
    "coord_dict = dict(_FillValue=False)\n",
    "for var in written.coords:\n",
    "    encoding[var].update(coord_dict)\n",
    "```\n",
    "\n",
    "## Unlimited dimensions\n",
    "\n",
    "Last but not least, one key element of netCDF is the **unlimited dimension**. You can set a *keyword argument* in the `to_netcdf` command. E.g., for rewriting a zarr-CMIP6 dataset into netCDF, consider compression and fillValue in the encoding and run\n",
    "\n",
    "```python\n",
    "written.to_netcdf(\"testcase.nc\",\n",
    "                  format=\"NETCDF4_CLASSIC\",\n",
    "                  unlimited_dims=\"time\",\n",
    "                 encoding=encoding)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1eff5a21-b2dd-43b6-9e00-88779638b6aa",
   "metadata": {},
   "source": [
    "<a class=\"anchor\" id=\"swiftstore\"></a>\n",
    "\n",
    "# Container handling with the swiftstore - `chmod`, `ls`, `rm`, `mv`"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "39f029d4-9efd-442d-8262-9ded55cf0a3d",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Saving a list of all zarr datasets\n",
    "\n",
    "You can get all `os_name`s of a container with `ls` of `fsspec`'s filesystem:\n",
    "\n",
    "```python\n",
    "all_zarr_datasets=container.fsspec_map.fs.ls(container.fsspec_map.root)\n",
    "print(all_zarr_datasets)\n",
    "```\n",
    "\n",
    "In case the data is free, you should save the list as follows:\n",
    "```python\n",
    "with fopen(\"zarrsets.txt\", \"w\") as f:\n",
    "    for os_name in all_zarr_datasets:\n",
    "        f.write(OS_STORAGE_URL+\"/\"+os_container+\"/\"+os_name)\n",
    "```\n",
    "This will enable to [simply open](#access) the zarr datasets with `xarray` afterwards."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "03a2f2e1-a4d7-4016-abe9-224663271a40",
   "metadata": {},
   "source": [
    "## How to make a container public\n",
    "\n",
    "- use tzis:\n",
    "\n",
    "```python\n",
    "%env OS_AUTH_TOKEN={token[\"OS_AUTH_TOKEN\"]}\n",
    "%env OS_STORAGE_URL={token[\"OS_STORAGE_URL\"]}\n",
    "tzis.toggle_public(\"testtzis\")\n",
    "```\n",
    "\n",
    "This will either make the container of the outstore *public* if it was not public or it will make it *private* by removing all access control lists if it was public. Note that only container as a whole can be made public or private.\n",
    "\n",
    "- With hand:\n",
    "\n",
    "1. Log in at https://swiftbrowser.dkrz.de/login/ . \n",
    "2. In the line of the target container, click on the arrow on the right side with the red background and click on `share`.\n",
    "3. Again, click on the arrow on the right side and click on `make public`."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "966d03c4-74a0-4f63-87ed-49ba6f4b29ae",
   "metadata": {},
   "source": [
    "## Remove a zarr-`store` i.e. all objects with `os_name` prefix\n",
    "\n",
    "- use the `store`:\n",
    "\n",
    "```python\n",
    "del container.fsspec_map[os_name]\n",
    "```\n",
    "\n",
    "- With hand:\n",
    "\n",
    "1. Log in at https://swiftbrowser.dkrz.de/login/ . \n",
    "2.\n",
    "    - On the line of the target container, click on the arrow on the right side and click on `Delete container`.\n",
    "    - Click on the target container and select the store to be deleted. Click on the arrow on the right side and click on `Delete`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "56bf4fef-58eb-413f-833a-20594b515fb4",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "tzisenvkernel",
   "language": "python",
   "name": "tzisenvkernel"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
