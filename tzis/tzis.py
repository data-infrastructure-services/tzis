#!/usr/bin/env python
# coding: utf-8

import fsspec
import copy
import zarr

# from zarrswift import SwiftStore
from collections import ChainMap
from swiftclient import Connection
from .swifthandling import *
from .daskhandling import *
from .rechunker import *
import xarray
import os
import shutil
import math
from tqdm import tqdm
from datetime import datetime, timedelta
from functools import partial
import requests
import stat
import sys
import time
import json
from .provenance import Provenance
from .catalog import *
from .openmf import *
from numcodecs import Blosc
import warnings

OPEN_ZARR_KWARGS = dict(consolidated=True, decode_cf=True, use_cftime=True)


# version:
# from xarray:
try:
    from importlib.metadata import version as _version
except ImportError:
    from importlib_metadata import version as _version  # type: ignore[no-redef]

try:
    __version__ = _version("tzis")
except Exception:
    # Local copy or not installed with setuptools.
    __version__ = "999"


class write_zarr(fsspec.FSMap):
    def __init__(
        self,
        fsmap,
        mf_dset,
        varname,
        verbose=False,
        chunkdim="time",
        target_mb=0,
        startchunk=0,
        validity_check=False,
        maxretries=3,
        trusted=True,
    ):
        self.DASK_CHUNK_SIZE = 70
        self._cratio = None
        self.original_size = None

        self.verbose = verbose

        self.fsspec_map = fsmap
        if not isinstance(fsmap, fsspec.FSMap):
            self.fsspec_map = fsspec.get_mapper(fsmap)
        self.fs = self.fsspec_map.fs

        self.varname = varname

        self.mf_dset = mf_dset
        self.provenance = None

        if isinstance(self.mf_dset, open_mfdataset_optimize):
            self.provenance = self.mf_dset.provenance
            self.original_size = self.mf_dset.provenance
        self.recent_chunk = None

        target = self.write_zarr_func(
            chunkdim=chunkdim,
            target_mb=target_mb,
            startchunk=startchunk,
            validity_check=validity_check,
            maxretries=maxretries,
            trusted=trusted,
        )
        super().__init__(target.root, target.fs)

    def _drop_vars_without_chunkdim(self, ds, chunkdim):
        """Drops all variables which do not depend on the chunk dimension.
        This is required because those variables cannot be written chunkwise with `write_by_region`.
        The coordinate `time_bnds` also has to be dropped
        because `xarray` does write it at first place when the dataset is initialized in the swift storage.

        Parameters
        ----------
        ds : `xarray` dataset
            `ds` is the `xarray` object returned by `xarray.open_mfdataset` and internally passed.
        chunkdim : {"time"}
            - time : Only dimension that is implemented yet

        Returns
        -------
        Dataset
            An `xarray` dataset without the dropped variables.
        """
        droplist = [var for var in ds.variables if chunkdim not in ds[var].coords]
        if chunkdim == "time" and "time_bnds" in ds.variables:
            droplist += ["time_bnds"]
        return ds.drop_vars(droplist)

    def _sel_range_for_chunk(self, ds, startindex, endindex, chunkdim):
        """Selects an interval spanned from `startindex` to `endindex` within the chunk dimension `chunkdim` from dataset `ds`.
        The indices are controlled in a loop and chosen such that exactly one chunk is matched.

        Parameters
        ----------
        ds : `xarray` dataset
             `ds` is the `xarray` object returned by `xarray.open_mfdataset` and internally passed.
        startindex : Integer
            the dimension index of the interval start
        endindex : Integer
            the dimension index of the interval end
        chunkdim : {"time"}            - time : Only dimension that is implemented yet

        Returns
        -------
        Dataset
            A subset `xarray` dataset of the original dataset.

        Raises
        ------
        ValueError
            If the chunk dimension is not supported.
        """
        if chunkdim == "time":
            return ds.isel(time=slice(startindex, endindex))
        else:
            return ds.isel(**{chunkdim: slice(startindex, endindex)})
            # raise ValueError(
            #    'Other chunk dimensions than "time" are not supported yet.'
            # )

    def _init_dataset(self, ds, store, chunkdim, overwrite):
        storestring, storage_options = get_storestring_and_options(store)
#        if overwrite:
#            try:
#                with self.fs.transaction:
#                    self.fs.rm(self.fsspec_map.root, recursive=True)
#                while self.fs.du(self.fsspec_map.root) != 0:
#                    time.sleep(0.02)
#                    continue
#            if self.fs.protocol != "swift":
#                with self.fs.transaction:
#                    self.fs.mkdirs(os.path.dirname(self.fsspec_map.root),exist_ok=True)
#            else:
#                if self.verbose:
#                    print("Make sure the container exists already!")
#            except Exception as e:
#                print(e)
#                if self.verbose:
#                    print("Could not remove target.")
#                pass
        try:
            # try fsspec store:
            ds.to_zarr(
                storestring,
                compute=False,
                consolidated=True,
                mode="w",
                storage_options=storage_options,
            )
            chunk_independent_list = [
                var for var in ds.variables if chunkdim not in ds[var].coords
            ]
            if self.verbose:
                print("The following variables are written when initializing:")
                print(chunk_independent_list)
            if chunk_independent_list:
                ds[chunk_independent_list].to_zarr(
                    storestring,
                    consolidated=True,
                    mode="r+",
                    storage_options=storage_options,
                )

        except:
            raise ValueError("Could not initialize dataset")
        return xarray.open_zarr(
            storestring, **OPEN_ZARR_KWARGS, storage_options=storage_options
        )

    def _open_or_initialize_swift_dset(
        self, store, ds, chunkdim, validity_check, overwrite=False
    ):
        storestring, storage_options = get_storestring_and_options(store)
        if not overwrite:
            try:
                return (
                    xarray.open_zarr(
                        storestring, **OPEN_ZARR_KWARGS, storage_options=storage_options
                    ),
                    False,
                )
            except:
                if validity_check:
                    raise ValueError(
                        f"validity_check=True but target dataset {self.fsspec_map.root} cannot be opened."
                    )
                elif self.verbose:
                    print("A new zarr dataset is created at store.")
                pass
        if self.verbose and overwrite:
            print("target dataset will be overwritten.")
        try:
            return self._init_dataset(ds, store, chunkdim, overwrite), True
        except Exception as e:
            raise RuntimeError(
                f"Could not initialize dataset: {e}"
                "\nTry to run `container.store.rmdir() and "
                "\ncontainer.open_store(zarr_dset)`"
            )

    def _check_for_overwrite(self, already, chunked_ds, varname, chunkdim):
        if self.verbose:
            print("Start checking if target must be overwritten")

        try:
            if already[varname].chunks != chunked_ds[varname].chunks:
                if self.verbose:
                    print(
                        f"""Chunk setting has changed from {already[varname].chunks} in target 
                        to {chunked_ds[varname].chunks} in your dataset."""
                    )
                return True
        except Exception as e:
            print(e)
            print("Chunk setting has changed.")
            return True

        already_coords = already.coords
        faulty_coords = []
        for coord in chunked_ds.coords:
            if chunkdim in coord:
                continue
            try:
                chunked_ds[coord].load()
                if not chunked_ds[coord].equals(already[coord]):
                    faulty_coords.append(coord)

            except:
                print(f"Could not check if {coord} is equal to target store.\n")
                # return True

        if faulty_coords:
            raise ValueError(
                "Source and target coordinates: \n"
                + ",".join(faulty_coords)
                + "\n differ."
            )

        a = already.attrs.copy()
        c = chunked_ds.attrs.copy()
        l_differ = False

        for key in ["tracking_id", "hasProvenance"]:
            if key in c.keys():
                del c[key]
            if key in a.keys():
                del a[key]

        if not a == c or l_differ:
            if self.verbose:
                akeys = a.keys()
                ckeys = c.keys()
                notinc = [key for key in akeys if key not in ckeys]
                notina = [key for key in ckeys if key not in akeys]
                print("Attributes of target store and source dataset differ.")
                if notina:
                    print(f"Attribute not in source: {','.join(notina)}")
                if notinc:
                    print(f"Attribute not in target: {','.join(notinc)}")
                if notina and notinc is not None:
                    for key in list(set(list(a.keys())) & set(list(c.keys()))):
                        if a[key] != c[key]:
                            print(f"Values of attribute {key} differ:")
                            print(f"source: {c[key]} target: {a[key]} ")
                # return True
        if self.verbose:
            print("target will not be overwritten. target will be appended if needed")

        return False

    def _check_for_equality(
        self,
        towrite,
        already,
        start_chunkdim_index,
        end_chunkdim_index,
        chunkdim,
        validity_check,
        chunk_no,
        varname,
    ):
        intarget = self._sel_range_for_chunk(
            already, start_chunkdim_index, end_chunkdim_index, chunkdim
        )  # .load()
        # if towrite.broadcast_equals(intarget):
        # if towrite[varname].size == intarget[varname].size :
        b_isidentical = towrite[varname].identical(intarget[varname])
        if intarget:
            intarget.close()
        if b_isidentical:
            if self.verbose:
                print(
                    f"Variable subset for index interval {start_chunkdim_index} - {end_chunkdim_index} "
                    f"of chunkdim {chunkdim} are equal."
                )
            # if not validity_check:
            #    print("Chunk {0} is skipped.".format(chunk_no + 1))
            return -1
        else :
            print(
                f"Variable subset for index interval {start_chunkdim_index} - {end_chunkdim_index} "
                f"of chunkdim {chunkdim} are different."
            )
            if self.verbose:
                print("to write:")
                print(towrite[varname])
                print("in target:")
                print(intarget[varname])
            if validity_check:
                return start_chunkdim_index
            return 0

    def _get_start_chunk(
        self,
        all_chunks,
        chunked_ds,
        chunksum,
        start_chunk,
        chunkdim,
        already,
        validity_check,
        varname,
    ):
        if start_chunk <= 0 and self.verbose:
            print(
                "Finding the start chunk can take long. You better provide the startchunk yourself"
            )
        chunked_ds_check = chunked_ds.copy()
        already_check = already.copy()

        if chunked_ds.coords.dtypes != already.coords.dtypes:
            if self.verbose:
                print(
                    "Dtypes of coords of target and write dset differ."
                    "Will try to remove coords before execute identical."
                )

            for coord in already.coords:
                try:
                    chunked_ds_check = chunked_ds_check.drop_vars(coord)
                    already_check = already_check.drop_vars(coord)
                except:
                    break

        for chunk_no in tqdm(range(0, len(all_chunks))):
            chunksum += all_chunks[chunk_no]
            if chunk_no < start_chunk:
                continue
            towrite = self._sel_range_for_chunk(
                chunked_ds_check, chunksum - all_chunks[chunk_no], chunksum, chunkdim
            )
            status_equality = self._check_for_equality(
                towrite,
                already_check,
                chunksum - all_chunks[chunk_no],
                chunksum,
                chunkdim,
                validity_check,
                chunk_no,
                varname,
            )
            if status_equality < 0:
                continue
            elif status_equality > 0:
                return status_equality, chunk_no
            chunksum -= all_chunks[chunk_no]
            break
        chunk_no += 1
        return 0, chunk_no

    def _reset_already_if_overwrite(
        self,
        validity_check,
        already,
        chunked_ds,
        store,
        isnew,
        chunkdim,
        varname,
        l_append,
    ):
        storestring, storage_options = get_storestring_and_options(store)
        b_isnew = isnew
        if not validity_check:
            overwrite = True
            if l_append:
                chunk_independent_list = []
                drop_list=[]
                for var in chunked_ds.variables:
                    if chunkdim not in chunked_ds[var].coords :
                        if ( var in already.variables )  and ( var != varname ) and ( chunkdim not in var ) and not  ( already[[var]].equals(chunked_ds[[var]]) ):
                            warnings.warn(
                                    f"Cannot append variable {var}"
                                    " because already written dataset contains it"
                                    f"{already[[var]]}"
                                    "while your dataset to be written looks different:"
                                    f"{chunked_ds[[var]]}"
                            )                                
                            drop_list.append(var)
                        elif chunkdim not in var :
                            chunk_independent_list.append(var)
                chunked_ds=chunked_ds.drop(drop_list)
                            
                    
                if chunk_independent_list:
                    chunked_ds[chunk_independent_list].to_zarr(
                        storestring,
                        consolidated=True,
                        mode="a",
                        storage_options=storage_options,
                    )

                chunked_ds[[varname]].to_zarr(
                    storestring,
                    compute=False,
                    consolidated=True,
                    mode="a",
                    storage_options=storage_options,
                )
                overwrite = False

            if overwrite:
                if b_isnew:
                    overwrite = False
                else:
                    overwrite = self._check_for_overwrite(
                        already, chunked_ds, varname, chunkdim
                    )
            if overwrite:
                already, dummy = self._open_or_initialize_swift_dset(
                    store, chunked_ds, chunkdim, validity_check, overwrite=True
                )
                b_isnew = True
        return already, b_isnew

    # `write_by_region` writes chunk-wise data into an initialized dataset in swift.
    # 'Initiliazed' means that a first `to_zarr` call has been executed which writes all coordinates and metadata for the dataset into the chunk. The subsequent `to_zarr` calls performed by `write_to_region` uses this information so that it knows how chunks have to be named. If a region has already been written, it will be overwritten by write_to_region.

    def write_by_region(self, towrite, store, chunkdim, chunkdimvalues):
        storestring, storage_options = get_storestring_and_options(store)
        towrite_chunks = towrite[chunkdim].values
        startindex = [
            idx
            for idx, chunkdimvalue in enumerate(chunkdimvalues)
            if chunkdimvalue == towrite_chunks[0]
        ]
        endindex = [
            idx
            for idx, chunkdimvalue in enumerate(chunkdimvalues)
            if chunkdimvalue == towrite_chunks[len(towrite_chunks) - 1]
        ]
        towrite.drop_vars(chunkdim).to_zarr(
            store=storestring,
            region={chunkdim: slice(startindex[0], endindex[0] + 1)},
            storage_options=storage_options,
        )  # ,
        return towrite

    def write_by_region_dask(
        self,
        istartchunk,
        all_chunks,
        chunked_ds,
        chunkdim,
        store,
        target_mb,
        varname,
        other_chunk_dims,
    ):

        client, b_islocalclient, work_dcs = get_client_and_config(
            target_mb, self.DASK_CHUNK_SIZE, self.verbose, other_chunk_dims
        )
        size = 0
        for start_dask_chunk in tqdm(range(istartchunk, len(all_chunks), work_dcs)):
            end_dask_chunk = start_dask_chunk + work_dcs
            if end_dask_chunk > len(all_chunks):
                end_chunk = sum(all_chunks)
                end_dask_chunk = len(all_chunks)
            else:
                end_chunk = sum(all_chunks[0:end_dask_chunk])
            start_chunk = sum(all_chunks[0:start_dask_chunk])
            to_write = self._sel_range_for_chunk(
                chunked_ds, start_chunk, end_chunk, chunkdim
            )
            starttime = time.time()
            # mapped_blocks=to_write.map_blocks(self.write_by_region,
            #                                  args=[store, chunkdim, chunked_ds[chunkdim].values],
            #                                  kwargs={},
            #                                  template=to_write)
            # result = mapped_blocks.compute()
            # del result
            self.write_by_region(to_write, store, chunkdim, chunked_ds[chunkdim].values)
            endtime = time.time()
            if self.verbose:
                assumed_mb = target_mb
                if self._cratio:
                    assumed_mb *= self._cratio
                if start_dask_chunk < 3 * work_dcs:
                    newsize = (
                        self.fs.du("/".join([self.fsspec_map.root, self.varname]))
                        / 1024
                        / 1024
                    )
                    assumed_mb = newsize - size
                    size = newsize
                    print(
                        "Stored data with {0}mb/s".format(
                            assumed_mb / (endtime - starttime)
                        )
                    )
                else:
                    print(
                        "Estimated writing speed {0}mb/s".format(
                            assumed_mb
                            * other_chunk_dims
                            * (end_dask_chunk - start_dask_chunk)
                            / (endtime - starttime)
                        )
                    )
            self.recent_chunk = start_dask_chunk
        if b_islocalclient:
            client.close()

        return 0

    def write_by_region_wrapper(
        self,
        chunked_ds,
        already,
        store,
        start_chunk,
        validity_check,
        chunkdim,
        varname,
        target_mb,
        is_safe_chunk,
        l_append,
        isnew=False,
    ):
        if self.verbose and validity_check:
            print("Reset target if overwrite and drop vars without chunkdim.")

        already, isnew = self._reset_already_if_overwrite(
            validity_check,
            already,
            chunked_ds,
            store,
            isnew,
            chunkdim,
            varname,
            l_append,
        )

        try:
            already = self._drop_vars_without_chunkdim(already, chunkdim)
        except:
            print(
                "Could not drop vars without chunkdim.",
                " This is not an issue if you initialized the dataset in the target.",
            )
        chunked_ds = self._drop_vars_without_chunkdim(chunked_ds, chunkdim)  #

        all_chunks = chunked_ds.chunks[chunkdim]
        chunksum = 0
        istartchunk = 0

        if self.recent_chunk:
            istartchunk = self.recent_chunk
        else:
            if not isnew and not validity_check and not l_append:
                if self.verbose:
                    print("Get start chunk")
                # different and validity:
                status_equality, istartchunk = self._get_start_chunk(
                    all_chunks,
                    chunked_ds,
                    chunksum,
                    start_chunk,
                    chunkdim,
                    already,
                    validity_check,
                    varname,
                )
                if status_equality > 0:
                    return status_equality

        if istartchunk < len(all_chunks):
            if self.verbose:
                print(f"Start Looping over chunks beginning with {istartchunk}")

            other_chunk_dims = calc_other_dim_chunks(
                dict(chunked_ds[varname].chunksizes.items()), chunkdim
            )

            if validity_check:
                client, b_islocalclient, work_dcs = get_client_and_config(
                    target_mb, self.DASK_CHUNK_SIZE, self.verbose, other_chunk_dims
                )

                for start_dask_chunk in tqdm(
                    range(istartchunk, len(all_chunks), work_dcs)
                ):
                    end_dask_chunk = start_dask_chunk + work_dcs
                    if end_dask_chunk > len(all_chunks):
                        end_dask_chunk = len(all_chunks)
                        end_chunkdim_index = sum(all_chunks)
                    else:
                        end_chunkdim_index = sum(all_chunks[0:end_dask_chunk])
                    start_chunkdim_index = sum(all_chunks[0:start_dask_chunk])

                    to_check = self._sel_range_for_chunk(
                        chunked_ds, start_chunkdim_index, end_chunkdim_index, chunkdim
                    )

                    status_equality = self._check_for_equality(
                        to_check,
                        already,
                        start_chunkdim_index,
                        end_chunkdim_index,
                        chunkdim,
                        validity_check,
                        0,  # dummy
                        varname,
                    )
                    if status_equality < 0:
                        continue
                    elif status_equality > 0:
                        if b_islocalclient:
                            client.close()
                        return status_equality
                if b_islocalclient:
                    client.close()

            else:
                try:
                    self.write_by_region_dask(
                        istartchunk,
                        all_chunks,
                        chunked_ds,
                        chunkdim,
                        store,
                        target_mb,
                        varname,
                        other_chunk_dims,
                    )
                except Exception as e:
                    print(e)
                    return 1
        return 0

    #def write_directly(self, dset=None, store=None):
    #    storestring, storage_options = get_storestring_and_options(store)
    #    dset.to_zarr(
    #        store=storestring,
    #        mode="w",
    #        consolidated=True,
    #        storage_options=storage_options,
    #    )

    def write_with_validation_and_retries(
        self,
        ds,
        varname,
        store,
        chunkdim,
        target_mb,
        startchunk,
        validity_check,
        maxretries,
        trusted,
    ):
        is_safe_chunk = True
        chunked_ds = ds

        if target_mb != 0:
            chunked_ds = rechunk(
                ds, varname, chunkdim, target_mb, self.verbose
            )
        elif target_mb == 0:
            orig_chunks_dict = dict(ds[varname].chunksizes.items())
            orig_chunks = [len(v) for k, v in orig_chunks_dict.items()]
            orig_no_of_chunks = 1
            for len_of_chunk in orig_chunks:
                orig_no_of_chunks *= len_of_chunk
            target_mb = ds[self.varname].nbytes / orig_no_of_chunks / (2**20)
            if self.verbose:
                print(f"original dask chunk size {target_mb}mb is used for chunking.")
        if target_mb > 2000:
            print(f"Warning: Your target_mb {target_mb} is too big for swift (max 2GB")

        #
        l_append = False
        retries = 0
        success = -1
        if startchunk != 0:
            success = startchunk

        already, b_isnew = self._open_or_initialize_swift_dset(
            store, chunked_ds, chunkdim, validity_check
        )
        if not b_isnew:
            if varname not in already.variables:
                if self.verbose:
                    print(
                        f"Varname {varname} not in target variables."
                        f"Will append {varname} to target store."
                    )
                l_append = True
        if l_append:
            coords_merge = list(set(list(already.coords) + list(chunked_ds.coords)))
            if coords_merge :
                if not already[coords_merge].identical(
                    chunked_ds[coords_merge]
                ):
                    raise ValueError("Cannot append if coords of variables differ")

        if l_append and validity_check:
            raise ValueError("Cannot validity check against new variable")
        # if validity_check and b_isnew GIVES a runtimerror from _open_or_initialize_swift_dset
        if not validity_check and not b_isnew and not l_append:
            if (chunked_ds[varname].nbytes / (1024 * 1024 * 1024)) < 4:
                if already.identical(chunked_ds):
                    if self.verbose:
                        print("The dataset is already completed in the target.")
                    return 0
            else:
                if self.verbose:
                    print(
                        "Could not compare target and source datasets because size of"
                        "source array is {0} GB"
                        "and therefore larger than internally set maximum of 4GB".format(
                            chunked_ds[varname].nbytes / (1024 * 1024 * 1024)
                        )
                    )
        if b_isnew and startchunk != 0:
            raise ValueError(
                "Cannot start at startchunk {0} because the zarr dataset is new.".format(
                    startchunk
                )
            )

        if validity_check:
            trusted = True
            print("Start validation")

        while success != 0 and retries < maxretries:
            success = self.write_by_region_wrapper(
                chunked_ds,
                already,
                store,
                success,
                validity_check,
                chunkdim,
                varname,
                target_mb,
                is_safe_chunk,
                l_append,
                isnew=b_isnew,
            )
            retries += 1
            if self.verbose and success != 0:
                print("Write by region failed. Now retry number {}.".format(retries))
        if success != 0:
            raise RuntimeError(
                "Max retries {0} all failed at chunk no {1}".format(maxretries, success)
            )
        if not trusted:
            if self.verbose:
                print("Start validation of write process")
            if (
                self.write_by_region_wrapper(
                    chunked_ds,
                    already,
                    store,
                    startchunk,
                    True,
                    chunkdim,
                    varname,
                    target_mb,
                    False,
                    is_safe_chunk,
                )
                != 0
            ):
                raise RuntimeError("Validiation failed.")

    def write_provenance(self, pargs):
        # try:
        if not self.provenance:
            self.provenance = Provenance(self.fsspec_map.root)
            if self.mf_dset.attrs:
                if "tracking_id" in self.mf_dset.attrs:
                    self.provenance.gen_input(
                        "input_tracking_id", self.mf_dset.attrs["tracking_id"]
                    )
                else:
                    first_attr = list(self.mf_dset.attrs.keys())[0]
                    self.provenance.gen_input(
                        f"input_{first_attr}", [str(self.mf_dset.attrs[first_attr])]
                    )
            elif self.verbose:
                print("Cannot create input provenance because no attrs given.")

        else:
            pargs["open_mfdataset_kwargs"] = json.dumps(OPEN_MFDATASET_KWARGS)
        self.provenance.gen_output(pargs)
        self.provenance.write_json()
        self.provenance.write_png()
        self.mf_dset.attrs.setdefault("hasProvenance", "")
        self.mf_dset.attrs["hasProvenance"] += " " + "/".join(
            [
                os.path.dirname(self.fsspec_map.root),
                "provenance",
                "_".join(
                    [
                        "provenance",
                        self.provenance.prefix,
                        self.provenance.tracking_id + ".json",
                    ]
                ),
            ]
        )
        # except Exception as e:
        #   print(e)
        #   print("Could not create full provenance")
        #   pass

    def _set_default_encoding(dset):
        attrs_or_encoding = ChainMap(dset.attrs, dset.encoding)
        coord_or_data_var = ChainMap(dset.attrs, dset.encoding)
        for var in coord_or_data_var:
            var_attrs_or_encoding =  ChainMap(dset[var].attrs, dset[var].encoding)
            if not "_FillValue" in var_attrs_or_encoding:
                dset[var].encoding["_FillValue"] = None
            if not "coordinates" in var_attrs_or_encoding:
                dset[var].encoding["coordinates"] = None
            if not "coordinates" in attrs_or_encoding:
                dset.encoding["coordinates"] = None
        return dset
        
    def write_zarr_func(
        self,
        chunkdim="time",
        target_mb=0,
        startchunk=0,
        validity_check=False,
        maxretries=3,
        trusted=True,
    ):
        if chunkdim not in self.mf_dset.coords:
            raise ValueError(
                f"Your specified chunkdim {chunkdim} is not available in the dataset {self.mf_dset}."
            )

        self.recent_chunk = None
        if not self.mf_dset:
            raise ValueError(
                "Do not know what to write. Please define object mf_dset first."
            )

        if not self.varname:
            self.varname = self._get_varname(self.mf_dset)
            if self.verbose:
                print(
                    "You did not specify a varname. Take the first data var out of the dset"
                    + f" which is {self.varname}"
                )

        self.mf_dset=self._set_default_encoding(self.mf_dset)
        
        for var in self.mf_dset.variables:
            if chunkdim in self.mf_dset[var].coords and chunkdim not in var and var != self.varname :
                self.mf_dset=self.mf_dset.drop(var)
        dset_to_write = self.mf_dset

        totalsize = dset_to_write.nbytes

        if self.original_size and not self._cratio:
            self._cratio = self.original_size / totalsize
            if self.verbose:
                print(f"Compression ratio was estimated by disk size to {self._cratio}")
        if "chunks" in dset_to_write[self.varname].encoding.keys():
            del dset_to_write[self.varname].encoding["chunks"]
        
        for var in dset_to_write.variables:
            l_set_compressor = False
            if "compressor" in dset_to_write[var].encoding.keys():
                try:
                    from gribscan import RawGribCodec
                    if isinstance(dset_to_write[var].encoding["compressor"],RawGribCodec):
                        l_set_compressor=True
                except Exception as e:
                    print(e)
                    pass
            else:
                l_set_compressor = True
                    
            if l_set_compressor:
                dset_to_write[var].encoding["compressor"]=Blosc(cname="zstd", clevel=5, shuffle=2)
        pargs = locals()

        if not validity_check:
            self.write_provenance(pargs)

        vars_with_chunkdim = [
            var
            for var in dset_to_write.variables
            if chunkdim in dset_to_write[var].coords
        ]
        if vars_with_chunkdim :
            self.write_with_validation_and_retries(
                dset_to_write,
                self.varname,
                self.fsspec_map,
                chunkdim,
                target_mb,
                startchunk,
                validity_check,
                maxretries,
                trusted,
            )
        #else:
        #    
        #    self.write_directly(dset_to_write, self.fsspec_map)
        #        self.mf_dset.close()

        #        try:
        write_index_file(
            self.fsspec_map.fs,
            os.path.dirname(self.fsspec_map.root),
            pattern=None,
            contact=None,
        )
        #        except Exception as e:
        #            print(e)
        #            print("Could not write index file")
        #            pass

        return copy.deepcopy(self.fsspec_map)
