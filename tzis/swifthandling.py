#!/usr/bin/env python
# coding: utf-8

import requests
import os
import os.path as osp
import getpass
from datetime import datetime
from pathlib import Path
import fsspec
import time
from copy import deepcopy
import stat
from swiftclient import Connection


async def get_client(**kwargs):
    import aiohttp
    import aiohttp_retry

    retry_options = aiohttp_retry.ExponentialRetry(
        attempts=3, exceptions={OSError, aiohttp.ServerDisconnectedError}
    )
    retry_client = aiohttp_retry.RetryClient(
        raise_for_status=False, retry_options=retry_options
    )
    return retry_client


def get_storestring_and_options(store):
    """ToDo: Check if string is required. If swift is the protocol, use aiohttp_retry client to
    overcome writing errors.

    Parameters
    ----------
    store : `fsspec` mapping.

    Returns
    -------
    storestring
        The root of the mapping as str.
    storage_options
        Backend options for to_zarr depending on the protocol as dictionary.
    """

    storage_options = None
    storestring = store
    if store.fs.protocol == "swift":
        storestring = store.root
        storage_options = {"get_client": get_client}

    return storestring, storage_options


def get_swift_mapper(url, os_token, container, os_name=None):
    """ """
    fs_url = get_fsspec_url(url)
    os_url = get_os_url(url)

    os.environ["OS_STORAGE_URL"] = os_url
    os.environ["OS_AUTH_TOKEN"] = os_token

    mapper_url = "/".join(list(filter(None, [fs_url, container, os_name])))
    fsspec_map = fsspec.get_mapper(mapper_url)
    mapper_mkdirs(fs_url, container, prefix=os_name)

    test_fsspec(fsspec_map)
    return fsspec_map


def mapper_mkdirs(url, container, prefix=None):
    mapper_url = "/".join(list(filter(None, [url, container, prefix])))
    fsmap = fsspec.get_mapper(mapper_url)
    # create container if necessary
    if fsmap.fs.protocol == "swift":
        getenv = os.environ.get
        conn = Connection(
            preauthurl=getenv("OS_STORAGE_URL"), preauthtoken=getenv("OS_AUTH_TOKEN")
        )
        print(f"Swifthandling checks if container '{container}' is in url '{url}' or creates it.")
        listings = fsmap.fs.listdir(url)
        if not container in [a["name"].split("/")[-1] for a in listings]:
            conn.put_container(container)

    else:
        with fsmap.fs.transaction:
            fsmap.fs.mkdirs(mapper_url, exist_ok=True)

    test_fsspec(fsmap)


def test_fsspec(fsmap):
    try:
        fsmap[".test"] = b"test"
        del fsmap[".test"]
        return fsmap
    except Exception as e:
        print(e)
        raise ValueError("Could not use fsspec implementation")


def get_fsspec_url(url):
    fs_url = deepcopy(url)
    if fs_url.startswith("https"):
        fs_url = fs_url.replace("https", "swift")
    elif fs_url.startswith("http"):
        fs_url = fs_url.replace("http", "swift")
    fs_url = fs_url.replace("v1/", "")
    return fs_url


def get_os_url(url):
    os_url = deepcopy(url)
    if os_url.startswith("swift"):
        os_url = os_url.replace("swift:/", "https:/")
    if not ".de/v1" in os_url:
        os_url = os_url.replace(".de/", ".de/v1/")
    return os_url


def _check_connection(swift_base, account, user, password=None):
    SWIFT_BASE = "https://swift.dkrz.de/"
    SWIFT_VERS = "v1.0"

    if swift_base == "jsc":
        SWIFT_BASE = "https://just-keystone.fz-juelich.de:8080/"
        SWIFT_VERS = "v1.0"
    elif swift_base != "dkrz":
        raise Exception("You have to provide either dkrz' or 'jsc' for swift_base")

    headers = {"X-Auth-User": f"{account}:{user}"}
    if password:
        headers["X-Auth-Key"] = password
    resp = requests.get(SWIFT_BASE + f"auth/{SWIFT_VERS}", headers=headers)
    try:
        resp.raise_for_status()
    except requests.RequestException:
        raise requests.RequestException(f"Connection to {SWIFT_BASE} failed")
    return resp


def _write_token(token, storage_url, expires, token_file="~/.swiftenv"):
    """Write credentials to a token file for permanent usage.

    Parameters:
    ==========
        token: str
            New swift login token
        storage_url: str
            url associated with a swift container and account
        expires: str
            expiry date of the token
        token_file : str, pathlib.Path, default: ~/.swiftenv
            filename where credentials are stored.
    """

    env = {
        "OS_AUTH_TOKEN": token,
        "OS_STORAGE_URL": storage_url,
        "OS_AUTH_URL": '" "',
        "OS_USERNAME": '" "',
        "OS_PASSWORD": '" "',
    }
    with open(osp.expanduser(token_file), "w") as f:
        f.write(f"#token expires on: {expires}\n")
        for key, value in env.items():
            f.write(f"setenv {key} {value}\n")
    os.chmod(osp.expanduser(token_file), stat.S_IREAD | stat.S_IWRITE)


def _check_token_for_user_and_account(account, username):
    try:
        with Path("~/.swiftenv_useracc").expanduser().open("r") as f:
            user = f.readline().strip().strip("\n").lower() == username.lower()
            acc = f.readline().strip().strip("\n").lower() == account.lower()
    except FileNotFoundError:
        user, acc = False, False
    with Path("~/.swiftenv_useracc").expanduser().open("w") as f:
        f.write(username + "\n")
        f.write(account + "\n")
    return all((user, acc))


def _get_envtoken(token_file="~/.swiftenv", **kwargs):
    """Read current swift credentials."""

    env = {}
    with open(os.path.expanduser(token_file)) as f:
        for nn, line in enumerate(f.readlines()):
            if nn == 0 and line.startswith("#"):
                date_str = line.strip("\n").partition(":")[-1].strip()
                expires = datetime.strptime(date_str, "%a %d. %b %H:%M:%S %Z %Y")
            if line.startswith("setenv"):
                content = line.strip("\n").partition(" ")[-1].split(" ")
                value = " ".join(content[1:]).strip('"').strip("'")
                env[content[0]] = value
    return env, expires


def create_token(swift_base, account, username, **kwargs):
    """Create a new swift token for a given account."""

    user = username or getpass.getuser()
    password = getpass.getpass(
        (f"Creating new swift-token" f"\nGive password for {user} on {account}: ")
    )
    resp = _check_connection(swift_base, account, user, password)
    tz = time.tzname[-1]
    expires = token = storage_url = None
    token = resp.headers["x-auth-token"]
    storage_url = resp.headers["x-storage-url"]
    expires_in = int(resp.headers["x-auth-token-expires"])
    expires_at = datetime.fromtimestamp(time.time() + expires_in)
    expires = expires_at.strftime("%a %d. %b %H:%M:%S {tz} %Y".format(tz=tz))
    _write_token(token, storage_url, expires, **kwargs)


def get_token(swift_base, account, username=None, **kwargs):
    """Check if a swift token for a given account is still valid.

    This method checks if a token for a swift container can be used
    to log on to swift and if the token is valid for at least one day.
    Otherwise a new token will be created.

    Parameters:
    ===========
        account : str
            Account name of the associated swift container
        username : str (default None)
            User name for the swift token, this system will get the
            username the runs this program if None is given (default)
    Returns:
    ========
        dict: Dictionary containing the swift credentials
    """

    try:
        env, expires = _get_envtoken(**kwargs)
    except FileNotFoundError:
        env, expires = {}, datetime.now()
    valid = _check_token_for_user_and_account(account, username or getpass.getuser())
    if (
        (expires - datetime.now()).total_seconds() / 60**2 > 24
        and env.get("OS_AUTH_TOKEN", "")
        and env.get("OS_STORAGE_URL")
        and valid
    ):
        return env
    create_token(swift_base, account, username or None)
    env, expires = _get_envtoken(**kwargs)
    return env


def toggle_public(container):
    "toggle container public read_acl settings"
    getenv = os.environ.get
    conn = Connection(
        preauthurl=getenv("OS_STORAGE_URL"), preauthtoken=getenv("OS_AUTH_TOKEN")
    )
    acl = conn.head_container(container).get("x-container-read", "")
    if ".r:*" in acl:
        acl = acl.replace(".r:*", "")
        acl = acl.replace(".rlistings", "")
    else:
        acl = ",".join([acl, ".r:*,.rlistings"])
    acl = ",".join(sorted(filter(None, set(acl.split(",")))))
    conn.post_container(container, headers={"X-Container-Read": acl})
