from dask.distributed import get_client, Client, system
from dask.system import CPU_COUNT
import math


def reset_dask_chunk_sizes(
    client, target_mb, verbose, DASK_CHUNK_SIZE, other_chunk_dims
):
    new_dsc = math.ceil(DASK_CHUNK_SIZE / other_chunk_dims)
    worker_info = client.scheduler_info()["workers"]
    avail_mem = (
        sum([worker["memory_limit"] for key, worker in worker_info.items()])
        / 1000
        / 1000
    )
    mem_per_work = avail_mem / len(worker_info)
    if mem_per_work < 10 * target_mb:
        print(
            f"""Note that your target_mb {target_mb} is too high for
              available memory per worker {mem_per_work}. That leads to
              memory leaks"""
        )
    required_mb = new_dsc * target_mb * 5
    if avail_mem < required_mb:
        new_dsc = math.ceil(avail_mem / (target_mb * 5))
        if verbose:
            print(
                f"""Reducing dask tasks size from {DASK_CHUNK_SIZE} to
                   {new_dsc}
                   because it requires about {required_mb} MB
                   while only {avail_mem} MB are available"""
            )
    return new_dsc


def get_client_and_config(target_mb, DASK_CHUNK_SIZE, verbose, other_chunk_dims):
    b_islocalclient = True
    try:
        client = get_client()
        b_islocalclient = False
    except:
        cpus = min(math.ceil(CPU_COUNT / 2), 4)
        client = Client(
            n_workers=cpus,
            # threads_per_worker=cpus,
            memory_limit=min(math.ceil(system.MEMORY_LIMIT / 2 / cpus), 20 * 10**9),
        )
    print(client)
    new_dsc = reset_dask_chunk_sizes(
        client, target_mb, verbose, DASK_CHUNK_SIZE, other_chunk_dims
    )
    return client, b_islocalclient, new_dsc
