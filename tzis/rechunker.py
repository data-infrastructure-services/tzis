import xarray
import math


def calc_chunk_length(ds, varname, chunkdim, target_mb, other_chunks):
    """Estimates the length of one chunk of `varname` of dataset `ds`in the chunk dimension `chunkdim`
    to match the targeted chunk size `target_mb` in the cloud.

    Parameters
    ----------
    ds : `xarray` dataset
         `ds` is the `xarray` object returned by `xarray.open_mfdataset` and internally passed.
    varname : string, user input
    chunkdim : {"time"}
        - time : Only dimension that is implemented yet
    target_mb : integer, user input
        The targeted size of one chunk in the cloud. The default is 1000.

    Returns
    -------
    Integer
        Size of one chunk in bytes

    Examples
    --------

    Raises
    ------
    """
    n_bytes = ds[varname].nbytes
    bytes_per_onestep=math.ceil(n_bytes / ( len(ds[chunkdim]) * other_chunks))
    if bytes_per_onestep > math.ceil(1.5*target_mb * (2**20)) :
        raise ValueError(
            f"You specified a chunkdim {chunkdim} which would contain {bytes_per_onestep/(2**20)}MB "
            f"which is more than the specified {target_mb}mb for variable {varname} per one step."
        )
    target_mb_matching_len=math.floor(
        len(ds[chunkdim]) * other_chunks / math.ceil(n_bytes / (target_mb * (2**20)))
    )
    #return_len=math.floor(len(ds[chunkdim])/(math.ceil(len(ds[chunkdim]) / target_mb_matching_len)))
    if target_mb_matching_len== 0:
        return 1
    return target_mb_matching_len
    

def calc_other_dim_chunks(orig_chunks_dict, chunkdim):
    other_dim_chunks = 1
    for k, v in orig_chunks_dict.items():
        chunklen = len(v)
        if k != chunkdim and chunklen > 1:
            other_dim_chunks *= chunklen
    return other_dim_chunks


def rechunk(ds, varname, chunkdim, target_mb, verbose):
    """rechunks the original dataset in order to have optimized chunk sizes.
    It sets the target chunk configuration with `xarray` function `chunk`
    and then loops over all variables in the dataset for unifying their chunks with `unify_chunks`.

    Parameters
    ----------
    ds : `xarray` dataset
         `ds` is the `xarray` object returned by `xarray.open_mfdataset` and internally passed.
    varname : string, user input
    chunkdim : {"time"}
        - time : Only dimension that is implemented yet
    target_mb : integer, user input
        The targeted size of one chunk in the cloud. The default is 1000.

    Returns
    -------
    Dataset
        An `xarray` dataset with unified and rechunked chunks according.
    """
    b_is_safe_chunk = True
    orig_chunks_dict = dict(ds[varname].chunksizes.items())
    orig_chunks = [len(v) for k, v in orig_chunks_dict.items()]
    orig_no_of_chunks = 1
    for len_of_chunk in orig_chunks:
        orig_no_of_chunks *= len_of_chunk
    orig_chunk_size = ds[varname].nbytes / orig_no_of_chunks / (2**20)
    if math.ceil(orig_chunk_size) < target_mb:
        print(
            "Target_mb {0} are larger than original chunk size {1}. "
            "Better only set target_mb to original size. "
            "Otherwise you get dask performance issues. ".format(
                target_mb, orig_chunk_size
            )
        )
        b_is_safe_chunk = False
        # return ds

    other_dim_chunks = calc_other_dim_chunks(orig_chunks_dict, chunkdim)
    if other_dim_chunks > 1 and verbose:
        print("Please consider: more than one dimension chunked.")

    chunk_length = calc_chunk_length(ds, varname, chunkdim, target_mb, other_dim_chunks)

    chunk_rule = {chunkdim: chunk_length}

    if verbose:
        print("Chunking into chunks of {0} {1} steps".format(chunk_length, chunkdim))
    chunked_ds = ds.chunk(chunk_rule)

    for var_id in chunked_ds.variables:
        chunked_ds[var_id].unify_chunks()

    return chunked_ds
