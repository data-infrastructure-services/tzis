import fsspec
import xarray
from .provenance import Provenance
from .rechunker import calc_chunk_length, calc_other_dim_chunks
import glob
import copy

OPEN_MFDATASET_KWARGS = dict(
    decode_cf=True,
    use_cftime=True,
    data_vars="minimal",
    coords="minimal",
    compat="override",
    combine_attrs="drop_conflicts",
)


class open_mfdataset_optimize:
    """
    Opens the dataset with xarrays `open_mfdataset`
    - with optimized chunks by estimating sizes with a test file `mf[0]` if `chunkdim` and `target_mb` are set.
    - with OPEN_MFDATASET_KWARGS and `xarray_kwargs` if provided

    It saves the original size of the source if available for estimating compression ratio.
    It initializes the provenance.
    It collects conflicting attributes and saves it as a new attribute.
    It sets a new `tracking_id` and appends to the history attribute.

    Parameters
    ----------
    mf : list or str
        mf is converted to a list and used as the first argument of `xarray.open_mfdataset`.
    varname: str
        varname is the variable which is used for rechunking and which should be written to the target storage.
    chunkdim=None : str
        chunkdim is the chunk dimension used for rechunking. Only set this in combination with target_mb.
    target_mb=None : int
        target_mb is the desired size of one chunk in the target storage in megabytes.
        Only set this in combination with chunkdim.
    xarray_kwargs=None : dict
        xarray_kwargs are unpacked within `open_mfdataset`.


    Returns
    -------
    Dataset or None
        The xarray dataset with optimized chunk setting and attributes.
    """

    def __init__(
        self,
        mf,
        varname,
        target_fsmap,
        chunkdim=None,
        target_mb=None,
        keep_auto_chunks=True,
        xarray_kwargs=None,
        verbose=False,
    ):

        # if type(mf) != list and type(mf) != str :
        #    raise ValueError("Dataset '{0}' must either be a string or a list of strings")
        self.mf = mf
        self.target_fsmap = target_fsmap
        if not isinstance(target_fsmap, fsspec.FSMap):
            self.target_fsmap = fsspec.get_mapper(target_fsmap)
        self.varname = varname
        self.mf_dset = None
        self.original_size = None
        self.provenance = None
        default_kwargs=copy.deepcopy(OPEN_MFDATASET_KWARGS)

        if verbose:
            print("Resetting disk size and compression ratio")

        imf = mf
        if type(mf) == str:
            imf = glob.glob(mf)
        if not imf:
            print(f"Could not glob {mf} - will pass it to xarray.")
            imf = copy.deepcopy(mf)

        if verbose:
            print("Opening test dataset")

        if xarray_kwargs is not None:
            default_kwargs.update(xarray_kwargs)

        try:
            testds = xarray.open_mfdataset(imf, **default_kwargs,chunks="auto")
        except:
            testds = xarray.open_mfdataset(imf, **default_kwargs)

        targetchunks={}
        for sourcechunkdim,chunklen in testds.chunks.items():
            targetchunks[sourcechunkdim]=chunklen[0]
            
        if not varname:
            varname = get_varname(testds)
        else:
            if varname not in list(testds.data_vars):
                raise ValueError(
                    "Given variable name {0} not in dataset.".format(
                        varname,
                    )
                )

        if verbose:
            print(f"Calculating size of {mf}")

        tempfs = fsspec.get_mapper(imf[0]).fs
        self.original_size = sum([tempfs.du(mf_input) for mf_input in imf])

        if verbose:
            print("Calculating chunk size")

        l_chunksset = False
        if chunkdim and target_mb:
            size = self.original_size / 1024 / 1024
            if size < target_mb:
                print(
                    f"In open_mfdataset_optimize we cannot set target_mb {target_mb} because "
                    f"the size of the test dataset {mf} is {size}MB and therefore smaller."
                )
            else:
                if keep_auto_chunks:
                    orig_chunks_dict = dict(testds[varname].chunksizes.items())
                    other_dim_chunks=calc_other_dim_chunks(orig_chunks_dict, chunkdim)
                    chunk_length = calc_chunk_length(
                        testds, varname, chunkdim, target_mb, other_dim_chunks
                    )
                    targetchunks.update({chunkdim:chunk_length})
                    if verbose:
                        print(f"Set chunks to {targetchunks}")

                    default_kwargs.update(dict(chunks=targetchunks))
                else:
                    chunk_length = calc_chunk_length(
                        testds, varname, chunkdim, target_mb, 1
                    )
                    if verbose:
                        print(f"Set chunks to {chunkdim}: {chunk_length}")
                    default_kwargs.update(dict(chunks={chunkdim:chunk_length}))
                l_chunksset = True

        testds.close()

        mf_dset = xarray.open_mfdataset(imf, **default_kwargs)  # ,

        if chunkdim and target_mb and l_chunksset:
            mf_dset = mf_dset.chunk({chunkdim: chunk_length})
            for var_id in mf_dset.variables:
                mf_dset[var_id].unify_chunks()

        conflict_attrs = get_conflict_attrs(imf, mf_dset, xarray_kwargs)

        self.provenance = Provenance(target_fsmap.root)

        if "tracking_id" in conflict_attrs:
            self.provenance.gen_input(
                "input_tracking_id", conflict_attrs["tracking_id"]
            )
        else:
            self.provenance.gen_input("input_file_name", imf)

        from .tzis import __version__ as tzis_version

        mf_dset.attrs["tracking_id"] = self.provenance.tracking_id
        mf_dset.attrs.setdefault("history", "")
        mf_dset.attrs[
            "history"
        ] += "Converted and written to swift target with tzis version {0}".format(
            tzis_version
        )

        # if keep_attrs and conflict_attrs:
        #    for k, v in conflict_attrs.items():
        #        mf_dset.attrs[k] = v

        #            for coord in mf_dset.coords:
        #                mf_dset[coord].load()

        self.mf_dset = mf_dset

    def __str__(self):
        return self.mf_dset.__str__()

    def __repr__(self):
        return self.mf_dset.__repr__()


def get_conflict_attrs(mf, mf_dset, xarray_kwargs):
    """
    Collects attributes which conflict within all single dsets in `mf`.
    It opens all elements of `mf` with `xarray.open_dataset` and collects
    attributes in a dictionary and their values in lists.

    Parameters
    ----------
    mf : list or str
        mf is converted to a list and used as the first argument of `xarray.open_mfdataset`.
    mf_dset: Dataset
        `mf_dset` is the `xarray` object returned by `xarray.open_mfdataset` which does not include
        the conflicting attributes.
    xarray_kwargs=None : dict
        xarray_kwargs are unpacked within `open_mfdataset`.


    Returns
    -------
    Dict
        All conflicting attributes and its values.
    """

    conflict_attrs = {}
    # try:
    maxdigits = len(str(len(mf)))
    digitstring = "{0:0" + str(maxdigits) + "}"
    for fileno, dset in enumerate(mf):
        if xarray_kwargs is not None:
            ds = xarray.open_dataset(dset, **xarray_kwargs)
        else:
            ds = xarray.open_dataset(dset)
        dset_attrs = ds.attrs
        missing_attrs = {k: v for k, v in dset_attrs.items() if k not in mf_dset.attrs}
        for k, v in missing_attrs.items():
            # attr_prefix=" File "+digitstring.format(fileno)+ ": "
            # if dset_attrs["tracking_id"]:
            # attr_prefix=" "+dset_attrs["tracking_id"] + ": "
            # conflict_attrs[k]=conflict_attrs[k] + attr_prefix + v + ","

            conflict_attrs.setdefault(k, [])
            conflict_attrs[k].append(v)
    # except:
    #    if verbose:
    #        print("Could not collect all attributes.")
    return conflict_attrs


def get_varname(mf_dset):
    return_varname = ""
    varlist = list(mf_dset.data_vars)
    for var in varlist:
        cinv = False
        for coord in ["ap", "b", "ps", "bnds"]:
            if coord in var:
                cinv = True
                break
        if cinv:
            continue
        return_varname = var
        break
    if not return_varname:
        raise ValueError(
            "Could not find any variable to write to swift. Please specify varname."
        )
    if verbose:
        print("We use variable {0} in case we need to rechunk.".format(return_varname))
    return return_varname
